import unittest
from enum import Enum

import toastedmarshmallow
from marshmallow import fields, ValidationError, Schema

from toastedmarshmallow_enum import EnumField


class DummyGender(Enum):
    MALE = 'MALE'
    FEMALE = 'GENDER'


class DummyGroupId(Enum):
    ELEVEN = 11
    TWENTY_TWO = 22


class DummyLevel(Enum):
    BEST = 'BEST'
    WORST = 'WORST'


class DummyUserSchema(Schema):
    class Meta:
        jit = toastedmarshmallow.Jit

    id = fields.String(allow_none=False)
    name = fields.String(allow_none=True)
    age = fields.Integer(allow_none=True)
    gender = EnumField(DummyGender, allow_none=False)
    group_id = EnumField(DummyGroupId, allow_none=True)
    level = EnumField(DummyLevel, default=DummyLevel.BEST, missing=DummyLevel.BEST, allow_none=True)


class DummyUser:
    def __init__(self, id, name=None, age=None, gender=None, group_id=None, level=None):
        self.id = id
        self.gender = gender
        self.age = age
        self.name = name
        self.group_id = group_id
        self.level = level

    def to_dict(self):
        schema = DummyUserSchema()
        return schema.dump(self).data

    @classmethod
    def from_dict(cls, data):
        schema = DummyUserSchema(strict=True)
        normalized_data = schema.load(data).data
        return DummyUser(**normalized_data)
    

class EnumFieldTests(unittest.TestCase):
    def setUp(self):
        self.dummy_user = DummyUser(
            name='Jane',
            age=34,
            gender=DummyGender.FEMALE,
            group_id=DummyGroupId.TWENTY_TWO,
            id='1')

        self.data = dict(
            id='1',
            name='Jane',
            age=34,
            gender=DummyGender.FEMALE.value,
            group_id=DummyGroupId.TWENTY_TWO.value)

    def test_init_should_fail_given_enum_class_is_not_an_enum(self):
        with self.assertRaises(TypeError):
            EnumField(str)

    def test_serialize(self):
        # when
        result = self.dummy_user.to_dict()

        # then
        self.assertEqual(result['gender'], DummyGender.FEMALE.value)
        self.assertEqual(result['group_id'], DummyGroupId.TWENTY_TWO.value)

    def test_deserialize(self):
        # when
        result = DummyUser.from_dict(self.data)

        # then
        self.assertEqual(result.gender, DummyGender.FEMALE)
        self.assertEqual(result.group_id, DummyGroupId.TWENTY_TWO)

    def test_serialize_should_return_none_given_enum_field_allows_none(self):
        # given
        self.dummy_user.group_id = None

        # when
        result = self.dummy_user.to_dict()

        # then
        self.assertIsNone(result['group_id'])

    def test_serialize_should_return_none_even_when_enum_field_does_not_allow_none_for_compatibility_reasons(self):
        # given
        self.dummy_user.gender = None

        # when
        result = self.dummy_user.to_dict()

        # then
        self.assertIsNone(result['gender'])

    def test_deserialize_should_return_none_given_enum_field_allows_none(self):
        # given
        self.data['group_id'] = None

        # when
        result = DummyUser.from_dict(self.data)

        # then
        self.assertIsNone(result.group_id)

    def test_deserialize_should_throw_givenEnum_field_does_not_allow_none(self):
        # given
        self.data['gender'] = None

        # when & then
        with self.assertRaises(ValidationError):
            DummyUser.from_dict(self.data)

    def test_serialize_should_return_missing_value_as_defined_in_schema(self):
        # when
        result = self.dummy_user.to_dict()

        # then
        self.assertEqual(result['level'], DummyLevel.BEST.value)

    def test_deserialize_should_return_default_value_as_defined_in_schema(self):
        # when
        result = DummyUser.from_dict(self.data)

        # then
        self.assertEqual(result.level, DummyLevel.BEST)

    def test_init_should_throw_given_default_parameter_is_not_an_enum(self):
        # when
        with self.assertRaises(ValueError) as e:
            EnumField(DummyLevel, default=DummyLevel.BEST.value)

        # then
        self.assertEqual(str(e.exception), f"Field DummyLevel: 'default' parameter should be an enum")

    def test_init_should_throw_given_missing_parameter_is_not_an_enum(self):
        # when
        with self.assertRaises(ValueError) as e:
            EnumField(DummyLevel, missing=DummyLevel.BEST.value)

        # then
        self.assertEqual(str(e.exception), f"Field DummyLevel: 'missing' parameter should be an enum")